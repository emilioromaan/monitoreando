# Monitoreando
**Equipo:** Emilio Román Sánchez, Sebastián Sedano Castañeda y Alma Rosa Belén Yáñez Martínez.

Levantamiento de un servicio de monitoreo, utilizando Prometheus y Grafana, que obtenga distintas métricas de un servicio web, en este caso el Insecure Bank.

> Branch exe-macm1 contiene modificaciones al archivo `docker-compose.yml` de la carpeta `Monitoreo` para poder ejecutarse en procesadores Apple Silicon.

## Documentación
La documentación del proyecto puede encontrarse [aquí](documentacion/01-implementacion-de-servicio.pdf).
Los videos de explicación de proyecto pueden encontrarse [aquí](documentacion/Videos/).

## Instrucciones de ejecución
Es necesario levantar los contenedores correspondientes página web del *Insecure Bank* y los contenedores del servicio de monitoreo por separado. Para cada uno, incluimos los comandos a ejecutar.

### Insecure-bank
Para lograr levantar el contenedor que hace visible la página web de *InsecureBank* es necesario ejecutar los siguientes comandos, dentro de la carpeta `insecure-bank`.

```Shell
docker network create t2_proxy
cmd/load-game-database
cmd/usuario-fuerza-bruta
make start
```

En caso de ser necesario, se puede ejecutar el comando `make stop` para finalizar la ejecución de los contenedores.

### Prometheus y grafana
Para levantar los contenedores que componen el servicio de monitoreo, es necesario ejecutar el siguiente comando desde la carpeta `Monitoreo`.

```Shell
docker-compose up --build
``` 

## Métricas obtenidas
En esta sección pueden observarse parte de las gráficas obtenidas al levantar los servicios que componen el proyecto.

### Vista general del *dashboard*
Los *dashboards* o tableros de información permiten desplegar distintas gráficas reelevantes respecto al uso de recursos correspondiente a cada contenedor. 
![Imagen dashboard](extras/dashboard.jpeg)

### Uso de memoria RAM
El uso de memoria RAM por contenedor es una de las gráficas que pueden desplegarse, acompañada de la cantidad exacta de memoria utilizada por cada contenedor.
![Imagen RAM](extras/uso-ram.jpeg)

### Uso de CPU
Se obtuvieron distintas gráficas respecto al uso de CPU por contenedor. 
Primero, se tiene la gráfica correspondiente al uso de CPU agregado de los contenedores.
![Imagen CPU](extras/uso-cpu.jpeg)

En la misma gráfica puede desplegarse el procentaje de uso de CPU correspondiente a cada contenedor en algún momento específico.
![Imagen porcentaje CPU](extras/uso-cpu-porcentajes.jpeg)

También pueden desplegarse gráficas separadas para contenedor. En este caso, se presenta el porcentaje de uso de CPU correspondiente al *Insecure Bank*.
![Imagen CPU Insecure Bank](extras/uso-cpu-insecure.jpeg)

### Tráfico de red 
Es posible observar el tráfico de red general, desplegando la información transmitida (color verde) y la información recibida (color rojo).
![Imagen tráfico](extras/trafico-red.jpeg)

Se puede observar el tráfico enviado por cada contenedor, así como el tráfico recibido. Estas gráficas permiten tener información extra sobre los patrones de comunicación entre cada uno de los contenedores.
![Imagen tráfico enviado](extras/trafico-enviado.jpeg)
![Imagen tráfico recibido](extras/trafico-recibido.jpeg)