Glosario
--------

**Credenciales**: nombre de usuario y contraseña.

**Cuenta de usuario**: identificación única de un usuario en el servicio que se asocia a sus datos personales y saldo.

**Datos personales**: cualquier información concerniente a una persona física identificada o identificable.

**Identificación oficial**: documento provisto por el gobierno que avala la identidad de un usuario.

**Operación**: depósito o transferencia de dinero que se refleja como un cambio en el saldo de las cuentas de usuario.

**Saldo**: La cantidad de dinero asignada a la cuenta de usuario.

Especificación Funcional
------------------------

1.  Un *Operador* puede respaldar los datos del sistema.

    1.  Un *Operador* puede tomar un respaldo de datos y cargarlos al sistema.

2.  Un *Analista* puede consultar un API en búsqueda de fraude.

    1.  El *Analista* puede buscar anomalías en las características de las operaciones
        y correlacionarlas con los datos personales no identificables
        de las cuentas de usuario asociadas a ellas.

3.  Un *Prospecto* puede generar una cuenta en el servicio desde su móvil.

    1.  Un *Prospecto* puede registrar una cuenta.

        1. El *Prospecto* proporciona sus datos personales y sus credenciales.

        2. El *Prospecto* proporciona una copia de su identificación oficial.

    2.  Un *Cajero* debe verificar la identificación oficial
        y aprobar el registro para que sea válido.

4.  Un *Usuario* puede acceder a su cuenta desde su móvil.

    1.  Un *Usuario* puede registrar y modificar sus datos personales.

    2.  Un *Usuario* puede enviar dinero disponible en el saldo de su cuenta
        a otra cuenta de usuario.

        1.  El *Usuario* solicita la operación desde su móvil.

        2.  El *Sistema* genera la operación y cambia los saldos asociados a las cuentas de usuario que corresponde.

        3.  El *Sistema* devuelve un identificador de la operación al *Usuario*.

    3.  Un *Usuario* puede consultar sus operaciones.

5.  Un *Cajero* puede registrar operaciones bancarias.

    1.  Un *Usuario* puede solicitar un depósito.

        1.  El *Usuario* entrega el dinero que depositará su cuenta.

        2.  Un *Cajero* debe registrar el depósito, que se reflejará en el saldo de la cuenta de usuario.

    1.  Un *Usuario* puede solicitar una transferencia.

        1.  El *Usuario* proporciona al *Cajero* el monto y la cuenta de usuario destino.

        2.  Un *Cajero* debe registrar el depósito, que se reflejará en el saldo de las cuentas de usuario que correspondan.

    1.  Un *Usuario* puede solicitar un retiro.

        1.  El *Usuario* proporciona al *Cajero* el monto del retiro.

        2.  Un *Cajero* debe registrar el retiro, que se reflejará en el saldo de la cuenta de usuario.

        3.  El *Cajero* debe entregar el dinero al *Usuario*.

6.  Un *Auditor* puede revisar la bitácora de las operaciones
    para resolver disputas y asignar responsabilidades si resultara apropiado.
