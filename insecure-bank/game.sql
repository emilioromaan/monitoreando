-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: ic18mbc
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Usuarios_Banco`
--

DROP TABLE IF EXISTS `Usuarios_Banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Usuarios_Banco` (
  `idUsuario` int NOT NULL AUTO_INCREMENT,
  `tipoUsuario` int NOT NULL,
  `contrasena` varchar(32) NOT NULL,
  `nombre` varchar(36) NOT NULL,
  `saldo` int NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuarios_Banco`
--

LOCK TABLES `Usuarios_Banco` WRITE;
/*!40000 ALTER TABLE `Usuarios_Banco` DISABLE KEYS */;
INSERT INTO `Usuarios_Banco` VALUES (1,1,'123456','Patrick',10000),(2,2,'','Usuario',100000),(3,3,'contraena','Gaby Uribe',5000),(4,4,'0000','Nala',0),(5,5,'1234','Isaac Odriozola',0),(6,6,'3602','Will Smith',0);
/* `idUsuario` `tipoUsuario` `contrasena` `nombre` `saldo` */
/*!40000 ALTER TABLE `Usuarios_Banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos`
--

DROP TABLE IF EXISTS `movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movimientos` (
  `idMovimiento` int NOT NULL AUTO_INCREMENT,
  `idUsuario` int NOT NULL,
  `idUsuarioOtro` int NOT NULL,
  `tipoMovimiento` enum('DEPOSITO','RETIRO') NOT NULL,
  `cantidad` float NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idMovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos`
--

LOCK TABLES `movimientos` WRITE;
/*!40000 ALTER TABLE `movimientos` DISABLE KEYS */;
/* `idMovimiento` `idUsuario` `idUsuarioOtro` `tipoMovimiento` `cantidad` `fecha` */
INSERT INTO `movimientos` VALUES (1,1,0,'DEPOSITO',10000,'2022-02-09 18:53:05'),(2,2,0,'DEPOSITO',100000,'2022-02-09 19:43:59'),(3,3,0,'DEPOSITO',5000,'2022-02-09 19:44:16');
/*!40000 ALTER TABLE `movimientos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-28  0:39:01
