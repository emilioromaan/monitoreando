<?php
if (isset($_GET['url'])) {
    $url=$_GET['url'];
} else {
    $url='';
}
//print $url;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Insecure Bank - Login</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="/assets/favicon.ico" />
    <!-- Bootstrap Icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!--bootstrap-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap/css/bootstrap.min.css">
    <!--fontawesome-->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-5.10.2/css/all.css">
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <!-- SimpleLightbox plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.css" rel="stylesheet" />
    <!-- Core theme CSS -->
    <link href="css/login.css" rel="stylesheet" />
</head>

<body>
    <!--Cuerpo del login-->
    <form action="./php/usuario/loguear.php" method="post">
        <div class="row justify-content-center
              justify-content-md-center" align="center">
            <div class="col-8 col-md-6 col-lg-5" id="ingreso">
                <br>
                <div class="col-md-12">
                    <div class="text-white">
                        <h4><b>InsecureBank</b></h4>
                        <span>
              <h6 class="letra"><b>Tu banco confiable.</b></h6>
            </span>
                    </div>

                </div>
                <div class="form-group">
                    <div class="inputWithIcon">
                    <input type="hidden" value='<?php echo $url?>' name="url">
                        <input name="cuenta" type="text" placeholder="No Tarjeta" id="us" autocomplete="off" maxlenght="7" required="yes" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
                    </div>
                    <p class="text-secondary">Número completo de tarjeta</p>
                </div>
                <div class="form-group">
                    <div class="inputWithIcon">
                        <input name="password" type="password" placeholder="Contraseña" maxlength="20" id="ctr" required="yes">
                        <i class="fas fa-key pass fa-lg fa-fw" aria-hidden="true"></i>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-md-12">
                        <button type="submit" id="ingresar" class="btn btn-danger float-right">Ingresar <i
                class="fas fa-sign-in-alt text-outline-danger"></i></button>
                    </div>
                </div>
            </div>
            <!--ingreso-->
        </div>
    </form>

    <!--Footer Insecure-->
    <footer class="text-center text-lg-end" id="pPag">
        <div class="Insecure float-left">
            <img src="assets/login/letraInsecure.png" class="Insecure">
        </div>
        <!-- Copyright -->
        <hr>
        <div class="text-center p-3">
            ©️ 2020-2022 InsecureBank Ciudad de México:<br> Prolongación Paseo de la Reforma 880, Lomas de Santa Fe, México C.P. 01219, CDMX. <br>
            <a href="operador.html">Operador.</a>

        </div>

    </footer>

</body>

</html>
