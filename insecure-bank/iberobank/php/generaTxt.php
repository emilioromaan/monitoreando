<?php
include "config.php";

//Link a la Base de datos
$link = mysqli_connect($cfgServer['host'], $cfgServer['user'], $cfgServer['password']) or die('Could not connect: ' . mysqli_error($link));
mysqli_select_db($link, $cfgServer['dbname']) or die("Could not select database");

//hacemos query de movimientos
$query_movimientos = "SELECT * FROM movimientos";
//hacemos query de usuarios banco
$query_usu_banco = "SELECT * FROM Usuarios_Banco";

//resultado de movimientos
$result_movimientos = mysqli_query($link, $query_movimientos) or die("Query 1 failed");
//resultado de usuarios banco
$result_usu_banco = mysqli_query($link, $query_usu_banco) or die("Query 2 failed");

if($result_usu_banco && $result_movimientos){

	//realiza un arreglo de los datos encontrados en el select movimientos.
	$array_movimientos = mysqli_fetch_array($result_movimientos, MYSQLI_NUM);
	//realiza un arreglo de los datos encontrados en el select usuarios banco.
	$array_usu_banco = mysqli_fetch_array($result_usu_banco, MYSQLI_NUM);

	var_dump($array_movimientos." ".$array_usu_banco);
	// Liberamos memoria
	mysqli_free_result($result_usu_banco);
	mysqli_free_result($result_movimientos);
	// Cerramos la conexion
	@mysqli_close($link);

	//nombre del txt por generar
	$file = 'operador.txt';

	//abre el archivo
	$fp = fopen($file, 'w'); //lo abre para escritura

	if (!$fp){
		echo "No se ha podido escribir el archivo";
		exit();
	}else{

		fwrite($fp, "tabla: Movimientos".PHP_EOL);
		//introduce los registros de Movimientos
		foreach ($array_movimientos as $clave => $valor) {
			fwrite($fp, $valor.PHP_EOL);
		}

		fwrite($fp, "tabla: Usuarios_Banco".PHP_EOL);
		//introduce los registros de Usuarios Banco
		foreach ($array_usu_banco as $clave => $valor) {
			fwrite($fp, $valor.PHP_EOL);
		}

		//cierra el archivo
		fclose($fp);
		echo("Archivo generado correctamente");
		exit();
	}
}else{
	echo ("Error al generar archivo txt");
	exit();
}
?>
