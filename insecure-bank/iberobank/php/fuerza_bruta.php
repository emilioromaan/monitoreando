<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Insecure Bank </title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="/assets/favicon.ico" />
    <!-- Bootstrap Icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
        rel="stylesheet" type="text/css" />
    <!-- SimpleLightbox plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="/css/styles.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
        <div class="container px-4 px-lg-5">
            <a class="navbar-brand" href="/#page-top">Insecure Bank</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto my-2 my-lg-0">
                    <li class="nav-item"><a class="nav-link" href="#login">Login</a></li>
                    <li class="nav-item"><a class="nav-link" href="#transferencia">Transferir</a></li>
                    <li class="nav-item"><a class="nav-link" href="#deposito">Deposito</a></li>
                    <li class="nav-item"><a class="nav-link" href="#retirar">Retirar</a></li>
                    <li class="nav-item"><a class="nav-link" href="/index.html">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
<?php
// print_r($_POST);
function debug($msg) {
    if (isset($_POST['debug'])) {
        echo '<p>DEBUG: $msg</p>';
    }
}
if(isset($_POST['cuenta'])) {
    $user=$_POST['cuenta'];
//     print_r('cuenta: '.$user);
} else {
    $user='';
//     print_r('cuenta: '.$user);
}
if(isset($_POST['password'])) {
    $provided_pass=$_POST['password'];
//     print_r('\nprovided_pass: '.$provided_pass);
} else {
    $provided_pass='';
//     print_r('\nprovided_pass: '.$provided_pass);
}
include "./../php/config.php";
$link = mysqli_connect(
  $cfgServer['host'],
  $cfgServer['user'],
  $cfgServer['password'])
  or die('Could not connect: ' . mysqli_error($link)
);
mysqli_select_db($link, $cfgServer['dbname']) or die("Could not select database");
$query = "SELECT contrasena FROM Usuarios_Banco WHERE idUsuario = '6';";
$result = mysqli_query($link, $query);
$pass= mysqli_fetch_assoc($result);
// print_r('pass: ');
// print_r($pass);

if ($user == 6 && $provided_pass == $pass['contrasena']) {
echo '
    <header class="masthead">
        <div class="container px-4 px-lg-5 h-100">
            <div class="row gx-4 gx-lg-5 h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-8 align-self-end">
                    <h1 class="text-white font-weight-bold">SUPERASTE FUERZA BRUTA...
                    </h1>
                    <hr class="divider" />
                </div>
                <div class="col-lg-8 align-self-baseline">
                    <p class="text-white-75 mb-5">FELICIDADES!!!</p>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row alert alert-warning m-5 text-center">
            <hr class="divider">
            <span>
            <iframe src="/msj.txt"></iframe>
            </span>
        </div>
    </div>

';
} else {
echo '
    <header class="masthead">
        <div class="container px-4 px-lg-5 h-100">
            <div class="row gx-4 gx-lg-5 h-100 align-items-center justify-content-center text-center">
                <div class="col-lg-8 align-self-end">
                    <h1 class="text-white font-weight-bold">Parece que entraste con SQL injection...</h1>
                    <hr class="divider" />
                </div>
                <div class="col-lg-8 align-self-baseline">
                    <p class="text-white-75 mb-5">O tal vez entraste directamente a esta página.</p>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row alert alert-warning m-5 text-center">
            <hr class="divider">
            <span>
            <p>Intenta hacer fuerza bruta con el usuario 6.</p>
            </span>
        </div>
    </div>
';
}
?>

    <!-- Footer-->
    <footer class="bg-light py-5">
        <div class="container px-4 px-lg-5">
            <div class="small text-center text-muted">Copyright &copy; 2022 - Insecure Bank</div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- SimpleLightbox plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</body>
</html>
