$(document).ready(function() {

    //Descargar datos del sistema
    $("#btnExportar").click(function() {
        $.ajax({
            type: "POST",
            url: "php/generaTxt.php",
            success: function(e) {
                $('#btnExportar').css("display", "none");
                $('#cont').append('<a href="operador.txt" download="operador.txt" >Descargar .txt</a> | ');
                $('#cont').append('<a href="operador.txt" target="_blank" >Ver .txt</a>');
            }
        });
    });

    //Cargar datos al sistema
    $("#btnAdjuntar").click(function() {
        //Si existe un nombre de archivo
        if ($("#customFile")[0].files[0]) {
            //se deshabilita el botón
            $("#btnAdjuntar").prop("disabled", true);
        } else {
            //se habilita el botón
            $("#btnAdjuntar").prop("disabled", false);
            alert("Selecciona un archivo para continuar");
        }
    });

}); //document ready
