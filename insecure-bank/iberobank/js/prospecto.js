$(document).ready(function() {
    //Registrar prospecto
    $("#btnRegistrar").click(function() {

        //tomar los valores de los inputs Nombre y contrasena
        const name = $("#name").val();
        const pass = $("#pass").val();
        console.log("nombre: " + name + " pass: " + pass);

        //poner expresión regular para validar
        const regExpLeterNumber = /^[A-ZÑ0-9.\"-\/]+(\s[A-ZÑ0-9.\"-\/]+)*\?$/;
        const regExpLeter = /^[A-ZÑ.\"-\/]+(\s[A-ZÑ.\"-\/]+)*\?$/;
        let error = false;

        /*
        //VALIDAMOS EL INGRESO
        if (!regExpLeter.test(name)) {
            error = true;
        }
        if (!regExpLeterNumber.test(pass)) {
            error = true;
        }
        */

        //si existe error
        if (name == "" || pass == "") {
            alert("Nombre o Contraseña inválido.");
            return;
        } else {
            //console.log(window.location);
            //alert("Enviando a php nombre:" + name + " contra:" + pass + ".");

            //mandamos la información a PHP
            $.ajax({
                data: {
                    "name": name,
                    "pass": pass,
                },
                type: "POST",
                dataType: "json",
                url: "php/prospecto.php"
            }).done(function(data) {
                alert(data);
            });

        }
        location.href = "../usuario.html";
    });

    //Enviar ID
    $("#btnEnviar").click(function() {
        const usuario = $("#name_cliente").val();

        if (!cliente) {
            alert("imposible validar cliente");
            return;
        } else {
            //llenamos input con valor de cliente
            $("#cliente").val(usuario);
            //hacemos llamado a cargaID.php
            $("#cargarImg").submit();
        }

    });

}); //document ready
