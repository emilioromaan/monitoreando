1. Para ejecutar aplicacion

	make start


2. Ejecutar este comando para generar la BD dentro de Docker.

	docker-compose exec -ti db mysql -u root -p210799 ic18mbc < banco.sql

3. EXPORTAR BD BANCO

	cmd/dump-database
